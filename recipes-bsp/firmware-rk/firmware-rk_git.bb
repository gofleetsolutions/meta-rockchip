# Copyright (C) 2016 - 2017 Jacob Chen <jacob2.chen@rock-chips.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Rockchip firmware"
DESCRIPTION = "Rockchip firmware such as for the WIFI, BT"

LICENSE = "BINARY"
LIC_FILES_CHKSUM = "file://readme.md;md5=cac11bf6ea3b10b9fcfc44ca748a9a44"
NO_GENERIC_LICENSE[BINARY] = "readme.md"

SRCREV = "${AUTOREV}"
SRC_URI = "git://github.com/rockchip-linux/rk-rootfs-build.git"
S = "${WORKDIR}/git"

inherit allarch

do_install () {
	install -d ${D}/system/etc/firmware/
	cp -rf ${S}/overlay-firmware/system/etc/firmware/* ${D}/system/etc/firmware/
}

PACKAGES =+ "${PN}-wifi \
	${PN}-bt \
"

FILES_${PN}-wifi = "/system/etc/*"